<?php

namespace App\Exceptions;

class LoginNotAuthorizedException extends \Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'Failed',
            'message' => 'Anda tidak memiliki hak akses ke halaman ini',
        ], 403);
    }
}
