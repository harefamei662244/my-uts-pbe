<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playlistsong extends Model
{
    /*
     * Mengambil playlist song user yang sedang Login
     */
    public static function getUserPlaylistSong($id)
    {
        $playlist = Playlist::find($id);
        $result = DB::table('playlistsongs')
            ->select('playlistId', 'playlists.name', 'songs.id as songs id', 'title', 'year',
                'artist', 'gendre', 'duration')
            ->join('playlists', 'playlists.id', '=', 'playlistsongs.playlistId')
            ->join('songs', 'songs.id', '=', 'playlistsongs.songId')
            ->where('playlistsongs.playlistId', '=', $playlist->id)
            ->get();
        return $result;
    }

    /*
     * Mengambil songs ID yang ada didalam playlistsongs
     */
    public static function getPlaylistSongId($id)
    {
        $result = DB::table('playlistsongs')
            ->join('songs', 'songs.id', '=', 'playlistsongs.songId')
            ->where('songs.id', '=', $id)
            ->first();
        return $result;
    }
}
