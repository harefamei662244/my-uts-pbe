<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    protected $hidden = ['password'];

    public static function loginVerify($email, $password)
    {
        $user = self::where('email', '=', $email)->first();
        if ($user !== NULL) {
            if (password_verify($password, $user->password)) {
                return $user;
            }
        }
        return false;
    }

    /*
     * Mengambil semua playlist user berdasarkan ID User
     */
    public static function getPlaylistUser($id)
    {

        $result = DB::table('playlists')
            ->select('playlists.id', 'name', 'userId')
            ->join('users', 'users.id', '=', 'playlists.userId')
            ->where('users.id', '=', $id)
            ->get();
        return $result;
    }

    /*
     * Mengambil semua playlistsongs user berdasarkan ID
     */
    public static function getUserPlaylistSong($id)
    {
        $playlist = Playlist::find($id);
        $result = DB::table('songs')
            ->select('users.id as users id', 'playlistId', 'playlists.name', 'songs.id as songs id', 'title', 'year',
                'artist', 'gendre', 'duration')
            ->join('playlistsongs', 'songs.id', '=', 'playlistsongs.songId')
            ->join('playlists', 'playlists.id', '=', 'playlistsongs.playlistId')
            ->join('users', 'users.id', '=', 'playlists.userId')
            ->where('playlistsongs.playlistId', '=', $playlist->id)
            ->get();
        return $result;
    }
}
