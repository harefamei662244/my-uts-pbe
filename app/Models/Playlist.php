<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playlist extends Model
{
    /*
     * Mengambil playlist dari user yang sedang Login
     */
    public static function getPlaylistUserLogin()
    {
        $result = DB::table('playlists')
            ->where('userId', '=', request()->user->id)
            ->get();
        return $result;
    }
}
