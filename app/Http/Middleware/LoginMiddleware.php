<?php

namespace App\Http\Middleware;

use App\Exceptions\LoginNotAuthtenticatedException;
use App\Models\User;
use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws LoginNotAuthtenticatedException
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('api_key'))) {
            /* Kondisi api token tidak dikirim melalui header */
            throw new LoginNotAuthtenticatedException();
        }
        /* Kondisi token tidak kosong */
        $token = request()->header('api_key');
        $user = User::where('api_key', '=', $token)->first();
        if ($user === null) {
            throw new LoginNotAuthtenticatedException();
        }
        /* Kondisi api tokennya ada */
        $request->user = $user;
        return $next($request);
    }
}
