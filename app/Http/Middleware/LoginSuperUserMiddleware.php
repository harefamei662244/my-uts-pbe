<?php

namespace App\Http\Middleware;

use App\Exceptions\LoginNotAuthorizedException;
use Closure;

class LoginSuperUserMiddleware
{
    public function handle($request, Closure $next)
    {
        if ($request->user->role !== 'superuser') {
            throw new LoginNotAuthorizedException();
        }
        return $next($request);
    }
}
