<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\LoginBaseController;
use App\Models\Playlistsong;
use App\Models\Song;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SongController extends LoginBaseController
{
    /**
     * Function untuk mendapatkan semua data Lagu
     * @return JsonResponse
     */
    public function getAllSong()
    {
        $songs = Song::all();
        return $this->successResponses(['songs' => $songs]);
    }

    /**
     * Mengambil songs berdasarkan ID
     * @param $id
     * @return JsonResponse
     */
    public function getSongById($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        return $this->successResponses(['song' => $song]);
    }

    /**
     * Fungsi untuk menambahkan lagu baru
     * @return JsonResponse
     */
    public function createSong()
    {
        /* Validasi jika salah inputan tidak diisi */
        $validasi = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required'
        ]);
        if ($validasi->fails()) {
            return $this->failResponse($validasi->errors()->getMessages(), 400);
        }
        //Jika semua inputan terisi
        $song = new Song();
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->successResponses(['song' => $song], 201);
    }

    /**
     * Digunakan untuk mengupdate songs berdasarkan ID
     * @param $id
     * @return JsonResponse
     */
    public function updateSong($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi jika salah inputan tidak diisi */
        $validasi = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required'
        ]);
        if ($validasi->fails()) {
            return $this->failResponse($validasi->errors()->getMessages(), 400);
        }
        //Jika semua inputan terisi
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->successResponses(['song' => $song], 201);
    }

    /**
     * Menghapus songs dengan ID tertentu yang tidak terdaftar didalam playlist
     * @param $id
     * @return JsonResponse
     */
    public function deleteSong($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        $playlistsong = Playlistsong::getPlaylistSongId($id);
        // Jika song id terdaftar didalam playlistsong
        if ($playlistsong !== null) {
            return $this->failResponse(['Message' => 'Song telah terdaftar didalam playlist'], 400);
        }
        // Jika song id tidak terdaftar didalam playlistsong
        $song->delete();
        return $this->successResponses(['song' => 'Data Berhasil Dihapus.']);
    }
}
