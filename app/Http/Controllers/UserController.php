<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\LoginBaseController;
use App\Models\Playlist;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use function PHPUnit\Framework\isEmpty;

class UserController extends LoginBaseController
{
    /**
     * Function untuk mendapatkan semua data User
     * @return JsonResponse
     */
    public function getAllUser()
    {
        $user = User::all();
        return $this->successResponses(['users' => $user]);
    }

    /**
     * Function untuk mengambil data user berdasarkan Primary Key
     * @param $id
     * @return JsonResponse
     */
    public function getUserById($id)
    {
        $user = User::find($id);
        //Jika user tidak ada didalam database
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        //Jika user ada didalam database
        return $this->successResponses(['user' => $user]);
    }

    /**
     * Fungsi untuk menambahkan user baru
     * @return JsonResponse
     */
    public function createUser()
    {
        /* Validasi jika salah inputan tidak diisi */
        $validasi = Validator::make(request()->all(), [
            'email' => 'required',
            'fullname' => 'required',
            'password' => 'required',
            'role' => 'required'
        ]);
        if ($validasi->fails()) {
            return $this->failResponse($validasi->errors()->getMessages(), 400);
        }
        //Jika semua inputan terisi
        $user = new User();
        $user->email = request('email');
        $user->fullname = request('fullname');
        $user->password = password_hash(request('password'), PASSWORD_DEFAULT);
        $user->role = request('role');
        $user->save();
        return $this->successResponses(['user' => $user], 201);
    }

    public function getUserPlaylistById($id)
    {
        $user = User::find($id);
        //Jika user tidak ada didalam database
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        // Mengambil semua playlist user berdasarkan ID user
        $playlist = User::getPlaylistUser($id);
        //Jika user ada didalam database
        return $this->successResponses(['user playlist' => $playlist]);
    }

    /**
     * Mendapatkan semua lagu dari playlistsongs berdasarkan ID user dan PlayslistsId
     * @param $id
     * @param $playlistId
     * @return JsonResponse
     */
    public function getUserPlaylistSongById($id, $playlistId)
    {
        $user = User::find($id);
        //Jika user tidak ada didalam database
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        /*
         * Mengecek playlist yang ada didalam database berdasarkan parameter
         */
        $playlist = Playlist::find($playlistId);
        if ($playlist === null) {
            return $this->failResponse([
                'Message' => 'Playlist tidak ditemukan'
            ], 400);
        }
        /* Validasi jika mencoba membuka playlists dari user lain */
        if ($playlist->userId !== $user->id) {
            return $this->failResponse([
                'Message' => 'Anda tidak bisa membuka playlist ini'
            ], 400);
        }
        $playlistSong = User::getUserPlaylistSong($playlistId);
        return $this->successResponses(['user playlist song' => $playlistSong]);
    }
}
