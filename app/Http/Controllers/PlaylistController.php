<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\LoginBaseController;
use App\Models\Playlist;
use App\Models\Playlistsong;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaylistController extends LoginBaseController
{
    /**
     * Mengambil playlists berdasarkan user yang sedang login
     * @return JsonResponse
     */
    public function getAllPlaylist()
    {

        $playlists = Playlist::getPlaylistUserLogin();
        return $this->successResponses(['playlists' => $playlists]);
    }

    /**
     * Mengambil playlists berdasarkan ID user
     * @param $id
     * @return JsonResponse
     */
    public function getPlaylistById($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi jika mencoba membuka playlists dari user lain */
        if ($playlist->userId !== request()->user->id) {
            throw new NotFoundHttpException();
        }
        return $this->successResponses(['playlist' => $playlist]);
    }

    /**
     * Membuat playlists baru berdasarkan user yang sedang login
     * @return JsonResponse
     */
    public function createPlaylist()
    {
        /* Validasi jika salah inputan tidak diisi */
        $validasi = Validator::make(request()->all(), [
            'name' => 'required',
        ]);
        if ($validasi->fails()) {
            return $this->failResponse($validasi->errors()->getMessages(), 400);
        }
        //Jika semua inputan terisi
        $playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->userId = request()->user->id;
        $playlist->save();
        return $this->successResponses(['playlist' => $playlist], 401);
    }

    /**
     * Menambahkan lagu ke dalam playlists yang sudah ada
     * Berdasarkan ID user yang sedang login
     * @param $id
     * @return JsonResponse
     */
    public function addSongToPlayslist($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi jika mencoba membuka playlists dari user lain */
        if ($playlist->userId !== request()->user->id) {
            return $this->failResponse([
                'Message' => 'Playlist tidak ditemukan'
            ], 400);
        }
        /* Validasi jika salah inputan tidak diisi */
        $validasi = Validator::make(request()->all(), [
            'id' => 'required|Exists:songs',
        ]);
        if ($validasi->fails()) {
            return $this->failResponse($validasi->errors()->getMessages(), 400);
        }
        //Jika semua inputan terisi
        $playlistSong = new Playlistsong();
        $playlistSong->songId = request('id');
        $playlistSong->playlistId = $playlist->id;
        $playlistSong->save();
        return $this->successResponses(['playlistsong' => $playlistSong], 401);
    }

    /**
     * Mengembalikan semua lagu yang ada didalam playlists
     * @param $id
     * @return JsonResponse
     */
    public function getSongPlayslist($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi jika mencoba membuka playlists dari user lain */
        if ($playlist->userId !== request()->user->id) {
            return $this->failResponse([
                'Message' => 'Playlist tidak ditemukan'
            ], 400);
        }
        /* Mengambil song yang ada didalam playlist */
        $playlistSong = Playlistsong::getUserPlaylistSong($id);
        return $this->successResponses(['playlistsong' => $playlistSong]);
    }
}
