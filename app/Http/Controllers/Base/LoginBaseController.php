<?php

namespace App\Http\Controllers\Base;

use Laravel\Lumen\Routing\Controller as BaseController;


abstract class LoginBaseController extends BaseController
{
    protected function successResponses(array $data, int $httpcode = 200)
    {
        $response = [
            'status' => 'Successed',
            'message' => 'Permintaan berhasil di proses',
            'data' => $data
        ];
        return response()->json($response, $httpcode);
    }

    protected function failResponse(array $data, int $httpcode)
    {
        $response = [
            'status' => 'Failed',
            'message' => 'Permintaan gagal di proses',
            'data' => $data
        ];
        return response()->json($response, $httpcode);
    }
}
