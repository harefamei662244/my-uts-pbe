<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function verify()
    {
        $email = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $user = User::loginVerify($email, $password);
        if ($user !== false) {
            $apiKey = md5($user->fullname . Str::random('12'));
            $user->api_key = $apiKey;
            $user->save();
            return $this->successResponses(['user' => $user]);
        }
        return $this->failResponse([], 401);
    }
}
