<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('app/v1/login', 'Auth\LoginController@verify');

$router->group(['prefix' => 'app/v1', 'middleware' => 'login.auth'], function ($router) {
    $router->group(['middleware' => 'login.superuser'], function ($router) {
        #User
        $router->get('/users', 'UserController@getAllUser');

        $router->post('/users', 'UserController@createUser');

        $router->get('/users/{id}', 'UserController@getUserById');

        /* User Playlists */
        $router->get('/users/{id}/playlists', 'UserController@getUserPlaylistById');

        /* User Playlists Song*/
        $router->get('/users/{id}/playlists/{playlistId}/songs', 'UserController@getUserPlaylistSongById');

        /* Songs */
        $router->post('/songs', 'SongController@createSong');

        $router->put('/songs/{id}', 'SongController@updateSong');

        $router->delete('/songs/{id}', 'SongController@deleteSong');

    });
    //Song
    $router->get('/songs', 'SongController@getAllSong');

    $router->get('/songs/{id}', 'SongController@getSongById');

    /* Playlist User */
    $router->get('/playlists', 'PlaylistController@getAllPlaylist');

    $router->get('/playlists/{id}', 'PlaylistController@getPlaylistById');

    $router->post('/playlists', 'PlaylistController@createPlaylist');

    $router->get('/playlists/{id}/songs', 'PlaylistController@getSongPlayslist');

    $router->post('/playlists/{id}/songs', 'PlaylistController@addSongToPlayslist');
});
